node[:deploy].each do |application, deploy|
  app_root = "#{deploy[:deploy_to]}/current"
  app_root << "/#{deploy[:document_root]}" unless deploy[:document_root].nil? || deploy[:document_root].empty?
  uploads_dir = "#{deploy[:deploy_to]}/shared/uploads"
  group = "www-data"
  group = "apache" unless `getent group apache`.empty?
  bash "fix_wp_uploads" do
    #user 'deploy'
    #environment ({ 'HOME' => ::Dir.home('deploy'), 'USER' => 'deploy' })
    #group 'www-data'
    #cwd "#{app_root}/wp-content"
    code <<-EOH
      if ! [ -d "#{uploads_dir}" ]; then
        mkdir "#{uploads_dir}"
      else
        find #{uploads_dir} -type d -exec chmod 777 {} \\;
      fi

      if [ -d "#{app_root}/wp-content/uploads" ]; then
        rm -rf "#{app_root}/wp-content/uploads"
      fi

      ln -s "#{uploads_dir}" "#{app_root}/wp-content/uploads"
      chown -R deploy:#{group} "#{app_root}/wp-content/uploads"

      EOH
    only_if { File.directory?("#{app_root}/wp-content") }
  end
end
