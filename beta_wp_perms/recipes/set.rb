node[:deploy].each do |application, deploy|
  app_root = "#{deploy[:deploy_to]}/current"
  app_root << "/#{deploy[:document_root]}" unless deploy[:document_root].nil? || deploy[:document_root].empty?
  group = "www-data"
  group = "apache" unless `getent group apache`.empty?
  bash "set_permissions" do
    user 'root'
    cwd "#{app_root}/wp-content"
    code <<-EOH
      mkdir -p uploads
      chmod -R 0777 uploads/
      chown -R deploy:#{group} uploads/
      EOH
    only_if { File.directory?("#{app_root}/wp-content") }
  end
end
