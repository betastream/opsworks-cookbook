# Required JSON for this to work:
# {
# "beta_fail2ban": {
#   "trusted_ips": [
#     "172.31.16.1",
#     "172.31.16.0/20"
#   ]
# }
# You have to populate that with the IP of the load balancer if you want the real user IP.
node[:deploy].each do |application, deploy|
    file '/etc/apache2/conf-available/remoteip.conf' do
        owner 'root'
        group 'root'
        mode '0644'
        action :create
        content 'RemoteIPHeader X-Forwarded-For' + 0x0D.chr + 0x0A.chr
    end

    node['beta_fail2ban']['trusted_ips'].each do |i|
        bash 'add_ip' do
            user 'root'
            code <<-EOH
                echo "RemoteIPTrustedProxy #{i}" >> /etc/apache2/conf-available/remoteip.conf
            EOH
            not_if "grep -q #{i} /etc/apache2/conf-available/remoteip.conf"
        end
    end

    bash 'change_logformat' do
        user 'root'
        code <<-EOH
            cp /etc/apache2/apache2.conf /etc/apache2/apache2.conf.old
        EOH
    end

    ruby_block "add options to the apache config" do
        block do
            fe = Chef::Util::FileEdit.new('/etc/apache2/apache2.conf')

            # Insert some options between an if statement
            fe.search_file_replace(/LogFormat "(.*)%h/, 'LogFormat "\1%a')
            fe.write_file
        end
    end

    bash 'mod_remoteip' do
        user 'root'
        code <<-EOH
            a2enmod remoteip
            a2enconf remoteip
            service apache2 reload
        EOH
        not_if do ::File.exists?('/etc/apache2/mods-enabled/remoteip') end
    end

    apt_package 'fail2ban'  do
        action :install
    end

    cookbook_file '/etc/fail2ban/filter.d/wordpress-hard.conf' do
        source 'wordpress-hard.conf'
        owner 'root'
        group 'root'
        mode '0644'
        action :create
    end

    cookbook_file '/etc/fail2ban/filter.d/apache-clientdenied.conf' do
        source 'apache-clientdenied.conf'
        owner 'root'
        group 'root'
        mode '0644'
        action :create
    end

    cookbook_file '/etc/fail2ban/filter.d/apache-404.conf' do
        source 'apache-404.conf'
        owner 'root'
        group 'root'
        mode '0644'
        action :create
    end

    template '/etc/fail2ban/jail.local' do
        source 'jail.local.erb'
        owner 'root'
        group 'root'
        mode '0644'
        action :create
        variables(
            :whitelist => node['beta_fail2ban']['trusted_ips'].join(' ')
        )
    end

    service 'fail2ban' do
        action :restart
    end
end
