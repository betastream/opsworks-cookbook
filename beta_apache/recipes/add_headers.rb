node[:deploy].each do |app_name, deploy|
    apache_conf = "/etc/apache2/apache2.conf"

    ruby_block "add options to the apache config" do
        block do
            fe = Chef::Util::FileEdit.new(apache_conf)

            # Insert some options between an if statement
            fe.insert_line_if_no_match("  Header always append X-Frame-Options SAMEORIGIN", "<IfModule headers_module>\n  Header always append X-Frame-Options SAMEORIGIN\n  Header always append X-XSS-Protection 1\n</IfModule>\n")
            fe.write_file

            # Insert some options at the end of the file
            fe.insert_line_if_no_match("TraceEnable Off", "TraceEnable Off")
            fe.write_file
            fe.insert_line_if_no_match("ServerSignature Off", "ServerSignature Off")
            fe.write_file
            fe.insert_line_if_no_match("ServerTokens ProductOnly", "ServerTokens ProductOnly")
            fe.write_file
        end
    end
end
