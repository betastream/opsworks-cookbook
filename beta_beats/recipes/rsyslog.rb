node[:deploy].each do |application, deploy|
    bash "install_rsyslog" do
        user 'root'
        code <<-EOH
            sudo apt-get update && sudo apt-get install rsyslog
        EOH
    end

    ruby_block "activate_rsyslog" do
        block do
            fe = Chef::Util::FileEdit.new('/etc/rsyslog.conf')

            fe.search_file_replace('#\$ModLoad imudp', '$ModLoad imudp')
            fe.search_file_replace('#\$UDPServerRun 514', '$UDPServerRun 514')
            fe.search_file_replace('#\$ModLoad imtcp', '$ModLoad imtcp')
            fe.search_file_replace('#\$InputTCPServerRun 514', '$InputTCPServerRun 514')
            fe.write_file
        end
    end

    # Add config to rsyslog
    cookbook_file '/etc/rsyslog.d/40-fastly.conf' do
        source 'fastly_rsyslog.conf'
        owner 'root'
        group 'root'
        mode '0644'
        action :create
    end

    # Add a logrotate config to delete old logs
    cookbook_file '/etc/logrotate.d/fastly' do
        source 'fastly_logrotate.conf'
        owner 'root'
        group 'root'
        mode '0644'
        action :create
    end

    bash "restart_rsyslog" do
        user 'root'
        code <<-EOH
            service rsyslog restart
        EOH
    end
end
