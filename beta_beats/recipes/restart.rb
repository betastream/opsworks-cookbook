node[:deploy].each do |application, deploy|
    bash "restart_filebeat" do
        user 'root'
        code <<-EOH
            service filebeat restart
        EOH
    end
end

node[:deploy].each do |application, deploy|
    bash "restart_topbeat" do
        user 'root'
        code <<-EOH
            service topbeat restart
        EOH
    end
end
